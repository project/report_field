<?php

namespace Drupal\report_field\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;

/**
 * Admin configuration setting form.
 */
class ReportFieldForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'report_field.adminsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'report_field_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $node_types = \Drupal::service('entity_type.manager')->getStorage('node_type')->loadMultiple();
    foreach ($node_types as $node_type_name => $node_type) {
      $bundle_fields = [];
      $i = 0;
      foreach (\Drupal::service('entity_field.manager')->getFieldDefinitions('node', $node_type_name) as $field_name => $field_definition) {
        if (!empty($field_definition->getTargetBundle())) {
          $bundle_fields[$i]['name'] = $field_name;
          $bundle_fields[$i]['label'] = $field_definition->getLabel();
          $i++;
        }
      }
      if (!empty($bundle_fields)) {
        $content_type = 'report_field_' . $node_type_name;
        $form[$content_type] = [
          '#type' => 'container',
          '#attributes' => [
            'id' => [$content_type],
          ],
        ];
        $form[$content_type]['text'] = [
          '#markup' => '<h2>' . $node_type->label() . '</h2>',
        ];
        foreach ($bundle_fields as $bundle_field_key => $bundle_field) {
          $item = 'report_field_' . $node_type_name . '_' . $bundle_field['name'];
          $form[$content_type][$item] = [
            '#prefix' => '<div id="' . $item . '">',
            '#suffix' => '</div>',
            '#tree' => TRUE,
            '#theme' => 'table',
            '#rows' => [],
            '#attributes' => [
              'class' => ['inner-table'],
              'id' => [$item],
            ],
          ];
          $title = Html::escape($bundle_field['label']);
          $checkbox = [
            '#type' => 'checkbox',
            '#title' => $title,
            '#default_value' => \Drupal::state()->get($item . '_checkbox', 0),
            '#attributes' => ['class' => ['report-field-field-checkbox']],
          ];
          $label = [
            '#type' => 'textfield',
            '#title' => t('Label'),
            '#default_value' => \Drupal::state()->get($item . '_label', ''),
            '#size' => 50,
            '#maxlength' => 255,
            '#states' => [
              'visible' => [
                ':input[name="' . $item . '[0][' . $item . '_checkbox]"]' => ['checked' => TRUE],
              ],
              'required' => [
                ':input[name="' . $item . '[0][' . $item . '_checkbox]"]' => ['checked' => TRUE],
              ],
            ],
          ];
          $reported = [
            '#type' => 'textfield',
            '#title' => t('Reported'),
            '#default_value' => \Drupal::state()->get($item . '_reported', ''),
            '#size' => 50,
            '#maxlength' => 255,
            '#states' => [
              'visible' => [
                ':input[name="' . $item . '[0][' . $item . '_checkbox]"]' => ['checked' => TRUE],
              ],
              'required' => [
                ':input[name="' . $item . '[0][' . $item . '_checkbox]"]' => ['checked' => TRUE],
              ],
            ],
          ];
          $message = [
            '#type' => 'checkbox',
            '#title' => t('Message field?'),
            '#default_value' => \Drupal::state()->get($item . '_message', ''),
            '#states' => [
              'visible' => [
                ':input[name="' . $item . '[0][' . $item . '_checkbox]"]' => ['checked' => TRUE],
              ],
            ],
          ];
          $form[$content_type][$item][] = [
            $item . '_checkbox' => &$checkbox,
          ];
          $form[$content_type][$item][] = [
            $item . '_label' => &$label,
            $item . '_reported' => &$reported,
            $item . '_message' => &$message,
          ];
          $form[$content_type][$item]['#rows'][] = [
            [
              'data' => &$checkbox,
              'colspan' => 3,
            ],
          ];
          $form[$content_type][$item]['#rows'][] = [
            ['data' => &$label],
            ['data' => &$reported],
            ['data' => &$message],
          ];
          unset($checkbox);
          unset($label);
          unset($reported);
          unset($message);
        }
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $values = $form_state->getValues();
    foreach ($values as $key => $value) {
      if (is_array($value)) {
        \Drupal::state()->set($key . '_checkbox', $value[0][$key . '_checkbox']);
        \Drupal::state()->set($key . '_label', $value[1][$key . '_label']);
        \Drupal::state()->set($key . '_message', $value[1][$key . '_message']);
        \Drupal::state()->set($key . '_reported', $value[1][$key . '_reported']);
      }
    }
  }

}
