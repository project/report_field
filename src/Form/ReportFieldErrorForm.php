<?php

namespace Drupal\report_field\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\user\Entity\User;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Crypt;

/**
 * Implements a report_field_error_form form.
 */
class ReportFieldErrorForm extends FormBase {

  /**
   * Error form displayed below the field.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   * @param string $label
   *   Checkbox label defined by admin in report_field_form.
   * @param int $flag_message
   *   If user can send message when reporting field.
   * @param string $field_name
   *   Name of the field.
   * @param string $field_title
   *   Title of the field.
   * @param string $bundle
   *   Content type where field appears.
   * @param string $nid
   *   Node ID.
   * @param string $title
   *   Node title.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $label = NULL, $flag_message = NULL, $field_name = NULL, $field_title = NULL, $bundle = NULL, $nid = NULL, $title = NULL) {
    $user = User::load(\Drupal::currentUser()->id());

    if (isset($_SESSION['report-field']['valid-email-no'])) {
      $_SESSION['report-field']['valid-email-no']++;
    }
    else {
      $_SESSION['report-field']['valid-email-no'] = 0;
    }

    $item = 'report_field_' . $bundle . '_' . $field_name . '_error';
    $form['error-form-wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'error-form-wrapper-' . $_SESSION['report-field']['valid-email-no'],
      ],
    ];
    $form['error-form-wrapper'][$item] = [
      '#type' => 'checkbox',
      '#title' => Html::escape($label),
      '#default_value' => 0,
    ];

    $form['error-form-wrapper']['valid-email'] = [
      '#markup' => '<div class="report-form-valid-email-' . $_SESSION['report-field']['valid-email-no'] . '"></div>',
    ];
    $form['error-form-wrapper']['email'] = [
      '#type' => 'textfield',
      '#title' => t('E-mail:'),
      '#default_value' => ($user->id() > 0) ? $user->getEmail() : '',
      '#access' => ($user->id() > 0) ? FALSE : TRUE,
      '#states' => [
        'visible' => [
          ':input[name="' . $item . '"]' => ['checked' => TRUE],
        ],
      ],
    ];
    if ($flag_message == 1) {
      $form['error-form-wrapper']['comment'] = [
        '#type' => 'textarea',
        '#title' => t('Message:'),
        '#default_value' => '',
        '#states' => [
          'visible' => [
            ':input[name="' . $item . '"]' => ['checked' => TRUE],
          ],
        ],
      ];
    }
    $form['error-form-wrapper']['error-form-wrapper-no'] = [
      '#type' => 'hidden',
      '#default_value' => $_SESSION['report-field']['valid-email-no'],
    ];
    $form['error-form-wrapper']['valid-email-no'] = [
      '#type' => 'hidden',
      '#default_value' => $_SESSION['report-field']['valid-email-no'],
    ];
    $form['error-form-wrapper']['field-name'] = [
      '#type' => 'hidden',
      '#default_value' => $field_name,
    ];
    $form['error-form-wrapper']['field-title'] = [
      '#type' => 'hidden',
      '#default_value' => $field_title,
    ];
    $form['error-form-wrapper']['content-type'] = [
      '#type' => 'hidden',
      '#default_value' => $bundle,
    ];
    $form['error-form-wrapper']['node-id'] = [
      '#type' => 'hidden',
      '#default_value' => $nid,
    ];
    $form['error-form-wrapper']['node-title'] = [
      '#type' => 'hidden',
      '#default_value' => $title,
    ];

    // Group submit handlers in an actions element with a key of "actions" so
    // that it gets styled correctly, and so that other modules may add actions
    // to the form. This is not required, but is convention.
    $form['error-form-wrapper']['actions'] = [
      '#type' => 'actions',
    ];

    // Add a submit button that handles the submission of the form.
    $form['error-form-wrapper']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
      '#states' => [
        'visible' => [
          ':input[name="' . $item . '"]' => ['checked' => TRUE],
        ],
      ],
      '#ajax' => [
        'callback' => '::reportFieldPageCallbackFunction',
      ],
    ];

    return $form;
  }

  /**
   * Getter method for Form ID.
   *
   * The form ID is used in implementations of hook_form_alter() to allow other
   * modules to alter the render array built by this form controller.  it must
   * be unique site wide. It normally starts with the providing module's name.
   *
   * @return string
   *   The unique ID of the form defined by this class.
   */
  public function getFormId() {
    return 'report_field_error_form';
  }

  /**
   * Implements form validation.
   *
   * The validateForm method is the default method called to validate input on
   * a form.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Implements a form submit handler.
   *
   * The submitForm method is the default method called for any submit elements.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Ajax submit function.
   */
  public function reportFieldPageCallbackFunction(array $form, FormStateInterface $form_state) {
    unset($_SESSION['report-field']['valid-email-no']);
    $user = User::load(\Drupal::currentUser()->id());
    $values = $form_state->getValues();
    // Check if user is registered or not.
    if ($user->id() == 0) {
      /* If not, status of the report will be pending, and email with
       * confirmation link will be sent to that user.
       */
      if (!\Drupal::service('email.validator')->isValid($values['email'])) {
        $response = new AjaxResponse();
        $response->addCommand(
          new ReplaceCommand('.report-form-valid-email-' . $values['valid-email-no'], t('Enter a valid email address.'))
        );
        return $response;
      }
      $status = 'pending';
      $to_encrypt = \Drupal::time()->getRequestTime() . Html::escape($values['email']);
      $secret_key = \Drupal::csrfToken()->get('86c6c7ff35b9979b151f2136cd13b0f2');
      $encrypted_message = Crypt::hmacBase64($to_encrypt, $secret_key);
      $params = [];
      $params['encrypted-message'] = $encrypted_message;
      $params['field'] = Html::escape($values['field-title']);
      $params['node'] = Html::escape($values['node-title']);
      \Drupal::service('plugin.manager.mail')->mail('report_field', 'validation', Html::escape($values['email']), \Drupal::languageManager()->getCurrentLanguage()->getId(), $params, NULL, TRUE);
    }
    else {
      /* If user is registered, delete all pending requests for that field and
       * mark field as reported.
       */
      $status = 'reported';
      \Drupal::database()->delete('report_field_invalid_fields')
        ->condition('reported_field', Html::escape($values['field-name']))
        ->condition('reported_content_type', Html::escape($values['content-type']))
        ->condition('node_id', Html::escape($values['node-id']))
        ->execute();
      $encrypted_message = '';
    }
    try {
      \Drupal::database()->insert('report_field_invalid_fields')->fields([
        'uid' => $user->id(),
        'email' => Html::escape($values['email']),
        'message' => Html::escape($values['comment']),
        'reported_field' => Html::escape($values['field-name']),
        'reported_field_title' => Html::escape($values['field-title']),
        'reported_content_type' => Html::escape($values['content-type']),
        'node_id' => Html::escape($values['node-id']),
        'node_title' => Html::escape($values['node-title']),
        'status' => $status,
        'hash' => $encrypted_message,
        'time' => \Drupal::service('date.formatter')->format(\Drupal::time()->getRequestTime(), 'medium', '', NULL, NULL),
      ])->execute();
    }
    catch (\PDOException $e) {
      $this->messenger()->addError(t('Error: %message', ['%message' => $e->getMessage()]));
    }
    $item = 'report_field_' . $values['content-type'] . '_' . $values['field-name'];
    $response = new AjaxResponse();
    $response->addCommand(
      new ReplaceCommand('#error-form-wrapper-' . $values['error-form-wrapper-no'], ($user->id() != 0) ? Html::escape(\Drupal::state()->get($item . '_reported', '')) : t('Please check your email and follow the confirmation link.'))
    );
    return $response;
  }

}
