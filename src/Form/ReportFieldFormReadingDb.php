<?php

namespace Drupal\report_field\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;

/**
 * Logs table form.
 */
class ReportFieldFormReadingDb extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'report_field.adminsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'report_field_form_reading_db';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $result = \Drupal::database()->select('report_field_invalid_fields', 'n')
      ->fields('n')
      ->execute()
      ->fetchAllAssoc('rid');
    $header = ['Rid', 'Uid', 'Email', 'Message', 'Reported Field',
      'Reported Field Title', 'Reported Content Type', 'Node ID', 'Node Title',
      'Status', 'Hash', 'Time', 'Delete Record',
    ];
    $form['records'] = [
      '#prefix' => '<div id="records">',
      '#suffix' => '</div>',
      '#tree' => TRUE,
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => [],
    ];
    if (!empty($result)) {
      foreach ($result as $db_record) {
        $rid = [
          '#markup' => $db_record->rid,
        ];
        $uid = [
          '#markup' => $db_record->uid,
        ];
        $email = [
          '#markup' => Html::escape($db_record->email),
        ];
        $message = [
          '#markup' => Html::escape($db_record->message),
        ];
        $reported_field = [
          '#markup' => $db_record->reported_field,
        ];
        $reported_field_title = [
          '#markup' => $db_record->reported_field_title,
        ];
        $reported_content_type = [
          '#markup' => $db_record->reported_content_type,
        ];
        $node_id = [
          '#markup' => $db_record->node_id,
        ];
        $node_title = [
          '#markup' => $db_record->node_title,
        ];
        $status = [
          '#markup' => $db_record->status,
        ];
        $hash = [
          '#markup' => $db_record->hash,
        ];
        $time = [
          '#markup' => $db_record->time,
        ];
        $checkbox = [
          '#type' => 'checkbox',
          '#title' => '',
          '#default_value' => 0,
        ];
        $form['records'][] = [
          'rid' => &$rid,
          'uid' => &$uid,
          'email' => &$email,
          'message' => &$message,
          'reported_field' => &$reported_field,
          'reported_field' => &$reported_field_title,
          'reported_content_type' => &$reported_content_type,
          'node_id' => &$node_id,
          'node_title' => &$node_title,
          'status' => &$status,
          'hash' => &$hash,
          'time' => &$time,
          $db_record->rid . '_checkbox' => &$checkbox,
        ];
        $form['records']['#rows'][] = [
          ['data' => &$rid],
          ['data' => &$uid],
          ['data' => &$email],
          ['data' => &$message],
          ['data' => &$reported_field],
          ['data' => &$reported_field_title],
          ['data' => &$reported_content_type],
          ['data' => &$node_id],
          ['data' => &$node_title],
          ['data' => &$status],
          ['data' => &$hash],
          ['data' => &$time],
          ['data' => &$checkbox],
        ];
        unset($rid);
        unset($uid);
        unset($email);
        unset($message);
        unset($reported_field);
        unset($reported_field_title);
        unset($reported_content_type);
        unset($node_id);
        unset($node_title);
        unset($status);
        unset($hash);
        unset($time);
        unset($checkbox);
      }
      $form['submit'] = [
        '#type' => 'submit',
        '#value' => t('Save'),
      ];
    }
    else {
      $no_record = [
        '#markup' => '<strong>' . t('NO REPORTED FIELDS FOUND') . '</strong>',
      ];
      $form['records']['#rows'][] = [
        ['data' => &$no_record, 'colspan' => 13],
      ];
      unset($no_record);
    }

    return $form;
  }

  /**
   * Implements form validation.
   *
   * The validateForm method is the default method called to validate input on
   * a form.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (isset($values['records'])) {
      $array = $values['records'];
      foreach ($array as $array_index) {
        while (($index = current($array_index)) !== FALSE) {
          if ($index == 1) {
            $key = key($array_index);
            $key_trimmed = str_replace('_checkbox', '', $key);
            \Drupal::database()->delete('report_field_invalid_fields')
              ->condition('rid', $key_trimmed)
              ->execute();
          }
          next($array_index);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
