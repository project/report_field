<?php

namespace Drupal\report_field\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Additional settings form.
 */
class ReportFieldAdditionalSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {

    return [
      'report_field.adminsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'report_field_additional_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['days'] = [
      '#type' => 'textfield',
      '#title' => t('Delete pending requests after:'),
      '#default_value' => \Drupal::state()->get('report_field_delete_pending_requests_time', 7),
      '#size' => 3,
      '#maxlength' => 3,
      '#required' => TRUE,
      '#description' => 'Number of days after which pending request will be deleted.',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Implements form validation.
   *
   * The validateForm method is the default method called to validate input on
   * a form.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (!is_int((int) ($values['days'])) || (is_int((int) ($values['days'])) && (int) ($values['days']) < 1)) {
      $form_state->setErrorByName('days', t('Number of days must be positive integer!'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $values = $form_state->getValues();
    \Drupal::state()->set('report_field_delete_pending_requests_time', $values['days']);
  }

}
