<?php

namespace Drupal\report_field\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * ReportFieldMailController class.
 */
class ReportFieldMailController extends ControllerBase {

  /**
   * Email confirmation.
   */
  public function reportFieldMailConfirm() {
    $user_hash = $_GET['confirm'];
    $result = \Drupal::database()->select('report_field_invalid_fields', 'n')
      ->fields('n')
      ->condition('hash', $user_hash)
      ->execute()
      ->fetchAssoc();
    \Drupal::database()->update('report_field_invalid_fields')
      ->fields([
        'status' => 'reported',
      ])
      ->condition('hash', $user_hash)
      ->execute();
    if ($result == 0) {
      \Drupal::messenger()->addMessage(t('Invalid hash!'), 'error');
    }
    else {
      \Drupal::messenger()->addMessage(t('Field @field successfully reported!', ['@field' => $result['reported_field_title']]), 'status');
    }
    $response = new RedirectResponse(\Drupal::request()->getBaseUrl());
    $response->send();
    return $response;
  }

}
