
Report Field
-----------------

Report invalid fields on the site. For example, a field that displays an e-mail
address or telephone number has a wrong address/number. This module allows users
to report that field as invalid. If a registered user reports a field, it will
be automatically marked as invalid, displaying the text entered in configuration
the form. For anonymous users, an e-mail with a confirmation link will be sent.
Important: The module works with fields attached to nodes!

 Similar modules
 ------

    * flag

 Differences with flag module
 ------

    * With flag module user can report content on node level. With report_field 
      module content can be reported on field level.

 Installation
 ------

    * Ensure JavaScript is enabled.

    * Like any other modules, put the "report_field" module folder in 
      the "modules" folder.

    * Go to "admin/modules" and enable "Report Field" module.

 Usage
 ------
    
    * After enabling the module, the configuration page will be available 
      (admin/config/content/report_field).

    * First tab, FIELD LIST, displays all fields for each content type available
      on the site. By clicking checkbox next to field name, report field will be
      available next to that field. Fill in 'Label' and 'Reported' textboxes and
      optionally check the 'Message' field checkbox. The 'Label' field defines
      the text displayed next to checkbox rendered below the field.
      'Reported' field defines text that is displayed after the field is
      reported. Finally if checked, the 'Message' field will allow the user to
      enter some comment when reporting an invalid field.

    * The second tab, REPORTED FIELDS, lists all fields that are reported by
      users, both by anonymous and registered once.

    * The third tab, ADDITIONAL SETTINGS, allows users to enter the number of
      days after which all pending requests will be deleted
      (the default value is 7 days).
